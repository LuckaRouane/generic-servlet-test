<%@page import="prod.*" %>
<%@page import="models.*" %>
<%@page import="java.util.HashMap" %>

<%
    ServletContext cnt=request.getServletContext();
    ModeleView mv=(ModeleView) cnt.getAttribute("modeleview");
    HashMap map=mv.getData();
    String descri=(String) map.get("descri");
    Departement[] liste=(Departement[]) map.get("listedept");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><% out.print(descri); %></title>
    </head>
    <body>
        <h2>Description : <% out.print(descri); %></h2>
        <table border="1">
            <thead>
                <td>IdDepartement</td>
                <td>Nomdepartement</td>
            </thead>
            <tbody>
            <% for(int i=0;i<liste.length;i++){ %>  
            <tr>
                <td><% out.print(liste[i].getIdDepartement()); %></td>
                <td><% out.print(liste[i].getNomDepartement()); %></td>
            </tr>
            <% } %>
            </tbody>
        </table>
        
    </body>
</html>
