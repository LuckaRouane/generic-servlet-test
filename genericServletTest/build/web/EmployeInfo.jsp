<%@page import="prod.*" %>
<%@page import="models.*" %>
<%@page import="java.util.HashMap" %>
<% 
    ServletContext cnt=request.getServletContext();
    ModeleView mv=(ModeleView) cnt.getAttribute("modeleview");
    HashMap map=mv.getData();
    String statusmessage=(String) map.get("statusmessage");
    int status=(int) map.get("status");
    Departement[] liste=null;
    String nom=null;
    if(status==1){
        nom=(String) map.get("nom");
        liste=(Departement[]) map.get("listedept");
    }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Employé info</title>
    </head>
    <body>
        <h1>Employé information</h1>
        <p>Status: <% out.print(statusmessage); %></p>
        <%if(status==1){ %>
        <p>Nom : <% out.print(nom); %></p>
        <p>Les departements que vous avez choisi sont </p>
        <table border="1">
            <thead>
                <td>IdDepartement</td>
                <td>Nomdepartement</td>
            </thead>
            <tbody>
            <% for(int i=0;i<liste.length;i++){ %>  
            <tr>
                <td><% out.print(liste[i].getIdDepartement()); %></td>
                <td><% out.print(liste[i].getNomDepartement()); %></td>
            </tr>
            <% } %>
            </tbody>
        </table>
        <% } %>
    </body>
</html>
