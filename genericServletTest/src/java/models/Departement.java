package models;

import java.util.HashMap;
import prod.AnnotationFonction;
import prod.ModeleView;

/**
 *
 * @author Lucka Rouane
 */
public class Departement {
    private int idDepartement;
    private String nomDepartement;

    public Departement(){}

    public Departement(String nomDepartement) {
        this.setNomDepartement(nomDepartement);
    }
    public int getIdDepartement() {
        return idDepartement;
    }
    
    public void setIdDepartement(int idDepartement) {
        this.idDepartement = idDepartement;
    }
    public String getNomDepartement() {
        return nomDepartement;
    }
    public void setNomDepartement(String nomDepartement) {
        this.nomDepartement = nomDepartement;
    }
    
    @AnnotationFonction(url="dept-add")
    public ModeleView add(){
        ModeleView mv = new ModeleView("listeDept.jsp");
        HashMap map=new HashMap();
        map.put("descri", "Departement added successfully");
        map.put("classname", this.getClass().getSimpleName());
        ModeleView listmv=list();
        HashMap listmap=listmv.getData();
        Departement[] listdeptmap=(Departement[]) listmap.get("listedept");
        Departement[] newlist=new Departement[listdeptmap.length+1];
        for(int i=0;i<newlist.length;i++){
            if(i==newlist.length-1){
                Departement de=new Departement(this.getNomDepartement());
                de.setIdDepartement(i);
                newlist[i]=de;
            }else{
                newlist[i]=listdeptmap[i];
            }
        }
        map.put("listedept", newlist);
        mv.setData(map);
        return mv;
    }
    
    @AnnotationFonction(url="dept-list")
    public ModeleView list(){
        ModeleView mv = new ModeleView("listeDept.jsp");
        HashMap map=new HashMap();
        map.put("descri", "Liste des departements");
        map.put("classname", this.getClass().getSimpleName());
        Departement[] listedept=new Departement[3];
        for(int i=0;i<listedept.length;i++){
            Departement de=new Departement("Departement no."+String.valueOf(i+1));
            de.setIdDepartement(i);
            listedept[i]=de;
        }
        map.put("listedept", listedept);
        mv.setData(map);
        return mv;
    }
    
    
}
