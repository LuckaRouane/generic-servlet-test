package models;

import java.util.HashMap;
import prod.AnnotationFonction;
import prod.ModeleView;

/**
 *
 * @author Lucka Rouane
 */
public class Employee {

    private int idEmployee;
    private String nom;
    private int[] idDepartement;

    public Employee(){}
    
    public int getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(int idEmployee) {
        this.idEmployee = idEmployee;
    }

    public int[] getIdDepartement() {
        return idDepartement;
    }

    public void setIdDepartement(int[] idDepartement) {
        this.idDepartement = idDepartement;
    }

    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
    @AnnotationFonction(url = "emp-add")
    public ModeleView add(){
        ModeleView mv = new ModeleView("EmployeInfo.jsp");
        HashMap map=new HashMap();
        String statusmessage="Vous n'avez choisi aucun departement";
        int status=0;
        map.put("classname", this.getClass().getSimpleName());
        int[] listchoice=this.getIdDepartement();
        if(listchoice!=null){
            status=1;
            statusmessage="Test réussi";
            Departement[] listeresp=new Departement[listchoice.length];
            for(int j=0;j<listchoice.length;j++){
                for(int i=0;i<3;i++){
                    if(i==listchoice[j]){
                        Departement de=new Departement("Departement no."+String.valueOf(i+1));
                        de.setIdDepartement(i);
                        listeresp[j]=de;
                    }
                }
            }
            map.put("listedept", listeresp);
            map.put("nom",this.getNom());
        }
        map.put("statusmessage", statusmessage);
        map.put("status", status);
        mv.setData(map);
        return mv;
    }

/*
    @AnnotationFonction(url = "emp-list")
    public ModeleView list() throws ClassNotFoundException {
        ModeleView mv = new ModeleView("context.jsp");        
        HashMap map=new HashMap();
        map.put("descri", "Liste des employees");
        map.put("classname", this.getClass().getSimpleName());
        mv.setData(map);
        return mv;
    }
*/
}
